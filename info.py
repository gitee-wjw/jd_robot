from os.path import dirname, join


class Info():
    # 京东平台参数
    jd_conut = ''
    jd_password = ''

    #1手动登录 / 0自动登录
    is_manual = 1

    # 运行时间
    run_date = ''  # 'today' 'tomorrow' or 2020-12-12
    run_time = ''  # h:m:s(例 20:00:01) or 'now'

    #商品链接
    goods = []

    # 其他信息
    ui_path = join(dirname(__file__), 'UI', 'index.html')
    resource_path = join(dirname(__file__), 'resource')
    driver_path = join(resource_path, 'chromedriver_win32', 'chromedriver.exe')
    temp_img_path = join(resource_path, 'temp', 'validate_img')
    cookie_path = join(resource_path, 'cookies', 'jd.cookies')
    proxies = []
