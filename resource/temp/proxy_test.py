import requests


def get_proxy_ips(proxy_ips=[]):
    url = 'https://ip.jiangxianli.com/api/proxy_ips'
    for i in range(100):
        proxies_data = requests.get(url).json()
        next_url = proxies_data['data']['next_page_url']
        if not next_url:
            break
        url = next_url
        if proxies_data['msg'] == '成功':
            for proxy_block in proxies_data['data']['data']:
                proxy_ips.append(proxy_block['ip'] + ':' + proxy_block['port'])
    return proxy_ips

def if_proxies_avaliable(proxy):
    proxies = {
        'http': 'http://' + proxy,
        'https': 'https://' + proxy
    }
    data = requests.get('http://ip-api.com/json', proxies=proxies).json()
    if data['status'] == 'success':
        return True
    return False

def if_ip_anonymous():
    url = 'http://httpbin.org/ip'

def test_selenium_proxy():
    pass


if __name__ == '__main__':
    # get_proxy_ips()
    if_proxies_avaliable()